package utilities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	@Test
	void testMatrixMethod() {
		int[][] original = {{1,2,3},{4,5,6}};
		int[][] copy = MatrixMethod.duplicate(original);
		int[][] expected = {{1,1,2,2,3,3},{4,4,5,5,6,6}};
		assertArrayEquals(expected, copy);
	}	

}
