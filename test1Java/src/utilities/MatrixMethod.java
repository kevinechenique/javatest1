package utilities;

public class MatrixMethod {
	
	public static int[][] duplicate(int[][] original){
		int originalCounter = 0;//v = value
		int[][] copy = new int[original.length][2*original[originalCounter].length];
		
		for(int i=0; i<copy.length; i++) {
			for(int j=0; j<copy[i].length; j++) {
				if(j%2==0 && j >1) {
					originalCounter -= 1;
				}
				else if(j%2!=0 && j >1) {
					originalCounter -= 1;
				}
				copy[i][j] = original[i][originalCounter];
				originalCounter++;
			}
		}
		
		return copy;
	}
}
