//Kevin Echenique Arroyo #1441258
package tests;
import automobiles.Car;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	/**
	  * Tests the get methods
	  */
	 @Test
	 /**
	  * Test if the method getSpeed returns the speed input in the constructor
	  */
	 void testGetSpeedMethod() {
	  Car c = new Car(10);
	  assertEquals(10, c.getSpeed());
	 }
	 
	 @Test
	 /**
	  * Tests if the exception is caught when negative speed is input in the constructor
	  */
	 public void testGetSpeedMethodException(){
	  try{
	   Car c = new Car(-4);
	   assertEquals(-4, c.getSpeed());
	  }
	  catch (IllegalArgumentException e) {
	   //Nothing to report here
	  }
	  catch (Exception e) {
	   fail("Speed cannot have negative values!");
	  }
	 }
	 
	 @Test
	 /**
	  * Tests the getLocation method and determines whether the car is in its initial position
	  */
	 void testGetLocationMethod() {
	  Car c = new Car(10);
	  assertEquals(50, c.getLocation());
	 }
	 
	 @Test
	 /**
	  * Tests the moveRight method and whether it exceeds the MAX_POSITION or not
	  */
	 void testMoveRightMethod() {
	  Car c1 = new Car(20);//should be fine
	  Car c2 = new Car(50);//Should reach the limit position
	  Car c3 = new Car(70);//should exceed the limit position and set value to 100
	  
	  c1.moveRight();
	  c2.moveRight();
	  c3.moveRight();
	  assertEquals(70, c1.getLocation());
	  assertEquals(100, c2.getLocation());
	  assertEquals(100, c3.getLocation());
	 }
	 
	 @Test
	 /**
	  * Tests the moveLeft method and whether it goes below the minimum value of 0 or not
	  */
	 void testMoveLeftMethod() {
	  Car c1 = new Car(20);//should be fine
	  Car c2 = new Car(50);//Should reach the minimum position of 0
	  Car c3 = new Car(70);//should exceed the minimum limit position and set value to 0
	  
	  c1.moveLeft();
	  c2.moveLeft();
	  c3.moveLeft();
	  assertEquals(30, c1.getLocation());
	  assertEquals(0, c2.getLocation());
	  assertEquals(0, c3.getLocation());
	 }
	 
	 @Test
	 /**
	  * Tests the accelerate method. It has to increase the speed by only 1
	  */
	 void testAccelerateMethod() {
	  Car c = new Car(10);
	  
	  c.accelerate();
	  assertEquals(11, c.getSpeed());
	 }
	 
	 @Test
	 /**
	  * Tests the stop method. It has to set speed to 0
	  */
	 void testStopMethod() {
	  Car c = new Car(50);
	  
	  c.stop();
	  assertEquals(0, c.getSpeed());
	 }

}
